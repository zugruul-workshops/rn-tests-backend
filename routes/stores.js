const express        = require('express')
const ResponseHelper = require('../Helpers/ResponseHelper')
const StoresController = require('../Controllers/StoresController')

const routerStores = express.Router()

//Fetch Routes
routerStores.get('/', (req, res) => {
    StoresController.fetchStores(req.query, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, false))
    })
})

//Find Routes
routerStores.get('/:id', (req, res) => {
    StoresController.findStore(req.params.id, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

//Add Routes
routerStores.post('/', (req, res) => {
    StoresController.addStore(req.body, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

//Update Routes
routerStores.put('/', (req, res) => {
    StoresController.updateStore(req.body, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

//Delete Routes
routerStores.delete('/:id', (req, res) => {
    StoresController.deleteStore(req.params.id, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

module.exports = routerStores