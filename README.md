# Node Mongoose

## Baixando o projeto e suas dependências:

```
git clone https://gitlab.com/zugruul-workshops/rn-tests-backend
cd rn-tests-backend
npm install
# ou yarn install
```

## Configuração do Database MongoDB
Criar um database chamado nosql_model com uma collection chamada de users

## Postman
Importar a collection e environment localizados na pasta config

## Rodando o Projeto:

Crie o database no MongoDB

No terminal rode:

```
npm start
```