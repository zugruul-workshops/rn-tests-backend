const constants = require('../config/contants')
const DAO       = require('../DAO/TransactionsDAO')
const sha256    = require('sha256')

class TransactionController {
    static fetchTransactions(query, callback) {
        const orderQuery = TransactionController.constructOrderQuery(query)
        const whereQuery = TransactionController.constructWhereQuery(query)
        
        return DAO.fetchTransactions(orderQuery, whereQuery, callback)
    }

    static findTransaction(idTransaction, callback) {
        return DAO.findTransaction(idTransaction, callback)
    }

    static addTransaction(newData, callback) {
        DAO.addTransaction(newData, callback)
    }

    static updateTransaction(dataToUpdate, callback) {
        DAO.updateTransaction(dataToUpdate, callback)
    }

    static deleteTransaction(idTransaction, callback) {
        DAO.deleteTransaction(idTransaction, callback)
    }

    static constructOrderQuery(query) {
         /**
         * Construção do ORDER BY:
         * 
         * isAscending: Define se a ordenação será ascendente ou descendente. (ASC ou DESC)
         * field: Define por qual atributo da tabela a esquisa será ordenada. Possíveis valores:
         * 
         *  Verifique a coleção do postman para um exemplo de uso desses campos.
         */
        let orderQuery = {}

        //Definição do valor de isAscending. Por padrão é ASC (Ascendente), se falso será DESC (Descendente).
        orderQuery.isAscending = query.isAscending === 'false'? '-1' : '1'

        switch(query.sort) {
            case '_id':
                orderQuery.field = '_id'
                break

            case 'id':
                orderQuery.field = '_id'
                break

            case 'created':
                orderQuery.field = 'createdAt'
                break

            case 'updated':
                orderQuery.field = 'updatedAt'
                break

            default: //Campo padrão da ordenação
                orderQuery.field = 'createdAt'
        }

        return orderQuery
    }

    static constructWhereQuery(query) {
        /** 
         * Construição do WHERE:
         * 
         * Possíveis parâmentros:
         * contains: procura pela string informada em todos os campos especificados na função constructWhereClause(), no arquivo TransactionsDAO.js
         * 
         * Verifique a coleção do postman para um exemplo de uso desse campo.
         */
        let whereQuery = {}
        if(query.contains !== undefined) {
            whereQuery.contains = query.contains
        }

        return whereQuery
    }
}

module.exports = TransactionController