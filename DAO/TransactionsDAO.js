const constants = require('../config/contants')
const models        = require('../models/index')

const TransactionModel = models['Transactions']
const UserModel = models['Users']

function fetchTransactions(orderQuery, whereQuery, callback) {
    const where = createWhereClause(whereQuery)
    const order = createOrderClause(orderQuery)

    TransactionModel.find(where).sort(order).exec((error, transactions) => {
        if(error) {
            let errorObj = { statusDesc: error, statusCode: constants.errorCodeMongoose }
            return callback(errorObj, null)
        }
        callback(null, transactions)
        return
    })
}

function findTransaction(id, callback) {
    TransactionModel
    .findById(id, (error, transaction) => { 
        if(error) {
            let errorObj = { statusDesc: error, statusCode: constants.errorCodeMongoose }
            return callback(errorObj, null)
        }
        if(!transaction) {
            // não há usuário com este id
            let errorObj = { statusDesc: constants.notFoundDesc, statusCode: constants.notFound }
            return callback(errorObj, null)
        }
        callback(null, transaction)
        return
    })
}

function addTransaction(transaction, callback) {
    const newTransactions = new TransactionModel(transaction)
    newTransactions.save((error, transaction) => {
        if(transaction) {
            return callback(null, transaction)
        } else {
            console.log(error)
            return callback(error, null)
        }
    })
}

function updateTransaction(newTransactionsData, callback) {
    // No banco é _id e não id!
    const id = newTransactionsData._id 
        ?  newTransactionsData._id
        : newTransactionsData.id

    if(!id) {
        let errorObj = { statusDesc: constants.notFoundDesc + '. Nenhum id foi fornecido!', statusCode: constants.notFound }
        return callback(errorObj, null)
    }
    
    TransactionModel
    .findByIdAndUpdate(newTransactionsData._id, newTransactionsData, (error, transaction) => {
        if(error) {
            let errorObj = { statusDesc: error, statusCode: constants.errorCodeMongoose }
            return callback(errorObj, null)
        }
        if(!transaction) {
            let errorObj = { statusDesc: constants.notFoundDesc, statusCode: constants.notFound }
            return callback(errorObj, null)
        } else {
            callback(null, transaction)
            return
        }
    })
}

function deleteTransaction(id, callback) {
    TransactionModel
    .findByIdAndRemove(id, (error, transaction) => {
        if(error) {
            return callback(error, null)
        }
        if(!transaction) {
            let errorObj = { statusDesc: constants.notFoundDesc, statusCode: constants.notFound }
            return callback(errorObj, null)
        } else {
            callback(null, transaction)
        }
    })
}

function createOrderClause(query) {  
    // Algoritmo para Sort de field unico, na controller monta-se a query para que ela seja transformada aqui.
    // Se quiserem sort de multiplos fields ver documentação do Mongoose e adaptar
    return {[query.field]: query.isAscending}
}

function createWhereClause(query) {
    if(query.contains !== undefined) {
        query.$or = [
            // equivalente à new RegExp(query.contains, "i")
        ]
    }
    delete query.contains

    return query
}

module.exports.fetchTransactions = fetchTransactions
module.exports.findTransaction   = findTransaction
module.exports.addTransaction    = addTransaction
module.exports.deleteTransaction = deleteTransaction
module.exports.updateTransaction = updateTransaction
