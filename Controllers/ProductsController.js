const constants = require('../config/contants')
const DAO       = require('../DAO/ProductsDAO')
const sha256    = require('sha256')

class ProductController {
    static fetchProducts(query, callback) {
        const orderQuery = ProductController.constructOrderQuery(query)
        const whereQuery = ProductController.constructWhereQuery(query)
        
        return DAO.fetchProducts(orderQuery, whereQuery, callback)
    }

    static findProduct(idProduct, callback) {
        return DAO.findProduct(idProduct, callback)
    }

    static addProduct(newData, callback) {
        DAO.addProduct(newData, callback)
    }

    static updateProduct(dataToUpdate, callback) {
        DAO.updateProduct(dataToUpdate, callback)
    }

    static deleteProduct(idProduct, callback) {
        DAO.deleteProduct(idProduct, callback)
    }

    static constructOrderQuery(query) {
         /**
         * Construção do ORDER BY:
         * 
         * isAscending: Define se a ordenação será ascendente ou descendente. (ASC ou DESC)
         * field: Define por qual atributo da tabela a esquisa será ordenada. Possíveis valores:
         * 
         *  Verifique a coleção do postman para um exemplo de uso desses campos.
         */
        let orderQuery = {}

        //Definição do valor de isAscending. Por padrão é ASC (Ascendente), se falso será DESC (Descendente).
        orderQuery.isAscending = query.isAscending === 'false'? '-1' : '1'

        switch(query.sort) {
            case '_id':
                orderQuery.field = '_id'
                break

            case 'id':
                orderQuery.field = '_id'
                break

            case 'name':
                orderQuery.field = 'name'
                break

            case 'price':
                orderQuery.field = 'price'
                break

            case 'description':
                orderQuery.field = 'description'
                break

            case 'created':
                orderQuery.field = 'createdAt'
                break

            case 'updated':
                orderQuery.field = 'updatedAt'
                break

            default: //Campo padrão da ordenação
                orderQuery.field = 'createdAt'
        }

        return orderQuery
    }

    static constructWhereQuery(query) {
        /** 
         * Construição do WHERE:
         * 
         * Possíveis parâmentros:
         * contains: procura pela string informada em todos os campos especificados na função constructWhereClause(), no arquivo ProductsDAO.js
         * 
         * Verifique a coleção do postman para um exemplo de uso desse campo.
         */
        let whereQuery = {}
        if(query.contains !== undefined) {
            whereQuery.contains = query.contains
        }

        return whereQuery
    }
}

module.exports = ProductController