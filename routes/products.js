const express        = require('express')
const ResponseHelper = require('../Helpers/ResponseHelper')
const ProductsController = require('../Controllers/ProductsController')

const routerProducts = express.Router()

//Fetch Routes
routerProducts.get('/', (req, res) => {
    ProductsController.fetchProducts(req.query, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, false))
    })
})

//Find Routes
routerProducts.get('/:id', (req, res) => {
    ProductsController.findProduct(req.params.id, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

//Add Routes
routerProducts.post('/', (req, res) => {
    ProductsController.addProduct(req.body, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

//Update Routes
routerProducts.put('/', (req, res) => {
    ProductsController.updateProduct(req.body, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

//Delete Routes
routerProducts.delete('/:id', (req, res) => {
    ProductsController.deleteProduct(req.params.id, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

module.exports = routerProducts