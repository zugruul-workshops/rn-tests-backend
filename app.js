const models     = require("./models")
const express    = require('express')
const bodyParser = require('body-parser')
const cors       = require('cors')

const loginRouter = require('./routes/login')
const usersRouter = require('./routes/users')
const productsRouter = require('./routes/products')
const transactionsRouter = require('./routes/transactions')
const storesRouter = require('./routes/stores')

const TokenManager = require('./Helpers/AuthManager')
var config = require('./config/config')

setupServer()

function setupServer() {
    const app = express()

    app.use(cors())
    app.use(bodyParser.json())
    app.use('/api/login', loginRouter)
    app.use('/api/users', 
        TokenManager.ensureUserToken,
         usersRouter)
    app.use('/api/products', TokenManager.ensureUserToken, productsRouter)
    app.use('/api/transactions', TokenManager.ensureUserToken, transactionsRouter)
    app.use('/api/stores', TokenManager.ensureUserToken, storesRouter)

    app.listen(config.app.port, function () {
        console.log(`Server listening on port ${config.app.port}`)
    })
}