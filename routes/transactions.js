const express        = require('express')
const ResponseHelper = require('../Helpers/ResponseHelper')
const TransactionsController = require('../Controllers/TransactionsController')

const routerTransactions = express.Router()

//Fetch Routes
routerTransactions.get('/', (req, res) => {
    TransactionsController.fetchTransactions(req.query, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, false))
    })
})

//Find Routes
routerTransactions.get('/:id', (req, res) => {
    TransactionsController.findTransaction(req.params.id, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

//Add Routes
routerTransactions.post('/', (req, res) => {
    TransactionsController.addTransaction(req.body, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

//Update Routes
routerTransactions.put('/', (req, res) => {
    TransactionsController.updateTransaction(req.body, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

//Delete Routes
routerTransactions.delete('/:id', (req, res) => {
    TransactionsController.deleteTransaction(req.params.id, (error, data) => {
        res.json(ResponseHelper.createResponse(error, data, true))
    })
})

module.exports = routerTransactions