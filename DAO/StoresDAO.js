const constants = require('../config/contants')
const models        = require('../models/index')

const StoreModel = models['Stores']
const UserModel = models['Users']

function fetchStores(orderQuery, whereQuery, callback) {
    const where = createWhereClause(whereQuery)
    const order = createOrderClause(orderQuery)

    StoreModel.find(where).sort(order).exec((error, stores) => {
        if(error) {
            let errorObj = { statusDesc: error, statusCode: constants.errorCodeMongoose }
            return callback(errorObj, null)
        }
        callback(null, stores)
        return
    })
}

function findStore(id, callback) {
    StoreModel
    .findById(id, (error, store) => { 
        if(error) {
            let errorObj = { statusDesc: error, statusCode: constants.errorCodeMongoose }
            return callback(errorObj, null)
        }
        if(!store) {
            // não há usuário com este id
            let errorObj = { statusDesc: constants.notFoundDesc, statusCode: constants.notFound }
            return callback(errorObj, null)
        }
        callback(null, store)
        return
    })
}

function addStore(store, callback) {
    const newStores = new StoreModel(store)
    newStores.save((error, store) => {
        if(store) {
            return callback(null, store)
        } else {
            console.log(error)
            return callback(error, null)
        }
    })
}

function updateStore(newStoresData, callback) {
    // No banco é _id e não id!
    const id = newStoresData._id 
        ?  newStoresData._id
        : newStoresData.id

    if(!id) {
        let errorObj = { statusDesc: constants.notFoundDesc + '. Nenhum id foi fornecido!', statusCode: constants.notFound }
        return callback(errorObj, null)
    }
    
    StoreModel
    .findByIdAndUpdate(newStoresData._id, newStoresData, (error, store) => {
        if(error) {
            let errorObj = { statusDesc: error, statusCode: constants.errorCodeMongoose }
            return callback(errorObj, null)
        }
        if(!store) {
            let errorObj = { statusDesc: constants.notFoundDesc, statusCode: constants.notFound }
            return callback(errorObj, null)
        } else {
            callback(null, store)
            return
        }
    })
}

function deleteStore(id, callback) {
    StoreModel
    .findByIdAndRemove(id, (error, store) => {
        if(error) {
            return callback(error, null)
        }
        if(!store) {
            let errorObj = { statusDesc: constants.notFoundDesc, statusCode: constants.notFound }
            return callback(errorObj, null)
        } else {
            callback(null, store)
        }
    })
}

function createOrderClause(query) {  
    // Algoritmo para Sort de field unico, na controller monta-se a query para que ela seja transformada aqui.
    // Se quiserem sort de multiplos fields ver documentação do Mongoose e adaptar
    return {[query.field]: query.isAscending}
}

function createWhereClause(query) {
    if(query.contains !== undefined) {
        query.$or = [
            // equivalente à new RegExp(query.contains, "i")
            { name:             { $regex: `${query.contains}`, $options: 'i'  }},
            { description:      { $regex: `${query.contains}`, $options: 'i'  }},
        ]
    }
    delete query.contains

    return query
}

module.exports.fetchStores = fetchStores
module.exports.findStore   = findStore
module.exports.addStore    = addStore
module.exports.deleteStore = deleteStore
module.exports.updateStore = updateStore
