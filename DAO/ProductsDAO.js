const constants = require('../config/contants')
const models        = require('../models/index')

const ProductModel = models['Products']

function fetchProducts(orderQuery, whereQuery, callback) {
    const where = createWhereClause(whereQuery)
    const order = createOrderClause(orderQuery)

    ProductModel.find(where).sort(order).exec((error, products) => {
        if(error) {
            let errorObj = { statusDesc: error, statusCode: constants.errorCodeMongoose }
            return callback(errorObj, null)
        }
        callback(null, products)
        return
    })
}

function findProduct(id, callback) {
    ProductModel
    .findById(id, (error, product) => {
        if(error) {
            let errorObj = { statusDesc: error, statusCode: constants.errorCodeMongoose }
            return callback(errorObj, null)
        }
        if(!product) {
            // não há usuário com este id
            let errorObj = { statusDesc: constants.notFoundDesc, statusCode: constants.notFound }
            return callback(errorObj, null)
        }
        callback(null, product)
        return
    })
}

function addProduct(product, callback) {
    const newProducts = new ProductModel(product)
    newProducts.save((error, product) => {
        if(product) {
            return callback(null, product)
        } else {
            callback(error, null)
            return
        }
    })
}

function updateProduct(newProductsData, callback) {
    // No banco é _id e não id!
    const id = newProductsData._id 
        ?  newProductsData._id
        : newProductsData.id

    if(!id) {
        let errorObj = { statusDesc: constants.notFoundDesc + '. Nenhum id foi fornecido!', statusCode: constants.notFound }
        return callback(errorObj, null)
    }
    
    ProductModel
    .findByIdAndUpdate(newProductsData._id, newProductsData, (error, product) => {
        if(error) {
            let errorObj = { statusDesc: error, statusCode: constants.errorCodeMongoose }
            return callback(errorObj, null)
        }
        if(!product) {
            let errorObj = { statusDesc: constants.notFoundDesc, statusCode: constants.notFound }
            return callback(errorObj, null)
        } else {
            callback(null, product)
            return
        }
    })
}

function deleteProduct(id, callback) {
    ProductModel
    .findByIdAndRemove(id, (error, product) => {
        if(error) {
            return callback(error, null)
        }
        if(!product) {
            let errorObj = { statusDesc: constants.notFoundDesc, statusCode: constants.notFound }
            return callback(errorObj, null)
        } else {
            callback(null, product)
        }
    })
}

function createOrderClause(query) {  
    // Algoritmo para Sort de field unico, na controller monta-se a query para que ela seja transformada aqui.
    // Se quiserem sort de multiplos fields ver documentação do Mongoose e adaptar
    return {[query.field]: query.isAscending}
}

function createWhereClause(query) {
    if(query.contains !== undefined) {
        query.$or = [
            // equivalente à new RegExp(query.contains, "i")
            { name:             { $regex: `${query.contains}`, $options: 'i'  }},
            { description:      { $regex: `${query.contains}`, $options: 'i'  }},
            { price:            { $regex: `${query.contains}`, $options: 'i'  }},
        ]
    }
    delete query.contains

    return query
}

module.exports.fetchProducts = fetchProducts
module.exports.findProduct   = findProduct
module.exports.addProduct    = addProduct
module.exports.deleteProduct = deleteProduct
module.exports.updateProduct = updateProduct
